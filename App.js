import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from "./android/app/components/Home";
import Detail from "./android/app/components/Detail";


const Stack = createStackNavigator();

function App() {
  const options = {
    headerStyle: {
      backgroundColor: '#2E4053',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} options={{...options, title: 'Inicio'}}/>
        <Stack.Screen name="Details" component={Detail} options={{...options, title: 'Detalle'}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App
